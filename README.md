# 建立自己的BaseActivity

[![GitHub release](https://img.shields.io/badge/release-v1.0.0-green.svg)](https://github.com/Thor-jelly/BaseClass/releases)

# 添加依赖

```
	allprojects {
		repositories {
			...
			maven { url 'https://jitpack.io' }
		}
	}


	dependencies {
	        compile 'com.github.Thor-jelly:BaseClass:v1.0.0'
	}

```


# 简单了解设计模式

[设计模式（设计模式概念,来自百度百科）](https://baike.baidu.com/item/%E8%AE%BE%E8%AE%A1%E6%A8%A1%E5%BC%8F/1212549?fr=aladdin)  
设计模式（Design Pattern）是一套被反复使用、多数人知晓的、经过分类的、代码设计经验的总结。
使用设计模式的目的：为了代码可重用性、让代码更容易被他人理解、保证代码可靠性。 设计模式使代码编写真正工程化；设计模式是软件工程的基石脉络，如同大厦的结构一样。  

**BaseActivity就是使用了模版设计模式**

# 系统中的模版设计模式
## 异步加载AsyncTask

- 为什么只能执行一次

```
@MainThread
public final AsyncTask<Params, Progress, Result> execute(Params... params) {
    return executeOnExecutor(sDefaultExecutor, params);
}
@MainThread
public final AsyncTask<Params, Progress, Result> executeOnExecutor(Executor exec,
        Params... params) {
    if (mStatus != Status.PENDING) {
        switch (mStatus) {
            case RUNNING:
                throw new IllegalStateException("Cannot execute task:"
                        + " the task is already running.");
            case FINISHED:
                throw new IllegalStateException("Cannot execute task:"
                        + " the task has already been executed "
                        + "(a task can be executed only once)");
        }
    }

    mStatus = Status.RUNNING;

    onPreExecute();

    mWorker.mParams = params;
    exec.execute(mFuture);

    return this;
}
```

## View绘制流程
**简单了解，如果有兴趣可以了解一下自定义view**

- 为什么RelativeLayout 不走onDraw方法
继承ViewGroup不调用onDraw()方法 ，但可以调用dispatchDraw方法来实现draw效果

```
final boolean dirtyOpaque = (privateFlags & PFLAG_DIRTY_MASK) == PFLAG_DIRTY_OPAQUE &&
             (mAttachInfo == null || !mAttachInfo.mIgnoreDirtyState);
             
在view源码draw方法中我们可以看到这句，就可以知道为什么不会被绘制了
**if (!dirtyOpaque) onDraw(canvas);**
```


<b>造成这种想象的原因是继承自RelativeLayout，而RelativeLayout这是一个容器，ViewGroup它本身并没有任何可画的东西，它是一个透明的控件，因些并不会触发onDraw； 

如果我们要重写ViweGroup的onDraw()方法，有两种方法： 

在构造函数里面，给其设置一个颜色，如#00000000； 

在构造函数里面，调用setWillNotDraw(false)，去掉其WILL_NOT_DRAW flag； </b>

# 基本构建

**只能放一些通用的方法，每个Activity中都会使用的方法，不是通用的写成工具类**  
分析一下哪些可以抽取的：  

- 一般我们会在onCreat()方法中设置layout
- 一般我们会在onCreat()方法中初始化view
- 一般我们会在onCreat()方法中初始化头部
- 一般我们会在onCreat()方法中初始化数据

```
/**
 * 类描述：基础Activity<br/>
 * 创建人：吴冬冬<br/>
 * 创建时间：2018/4/12 17:44 <br/>
 */
public abstract class BaseActivity extends AppCompatActivity {

    private static final String TAG = "123===";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "--------------------------------------------------------------------");
            Log.d(TAG, "当前Activity：" + this.getClass().getSimpleName() + "---->>>onCreate()");
            Log.d(TAG, "--------------------------------------------------------------------");
        }

        //设置布局
        setContentView(getContentView());

        //初始化p层，只有mvp模式才需要
        //initPresenter();

        //初始化界面
        initView();

        //初始化头部
        initTitle();

        //初始化点击事件，如果用注解注入方式的就不需要该方法
        initEvent();

        //初始化数据
        initData();
    }

    /**
     * 初始化数据
     */
    protected abstract void initData();

    /**
     * 初始化点击事件，如果用注解注入方式的就不需要改方法
     */
    protected abstract void initEvent();

    /**
     * 初始化头部
     */
    protected abstract void initTitle();

    /**
     * 初始化界面
     */
    protected abstract void initView();

    /**
     * 设置布局
     * @return R.layout.xxx
     */
    protected abstract @LayoutRes
    int getContentView();

    /**
     * 启动activity，如果需要传餐就需要在要跳转的类中自己写一个静态方法跳转到该类
     * @param clazz
     */
    protected void startActivity(Class<?> clazz){
        Intent intent = new Intent(this, clazz);
        startActivity(intent);
    }

//    /**
//     * 启动activity，如果需要传餐就需要在要跳转的类中自己写一个静态方法跳转到该类
//     * @param clazz
//     */
//    不经常用的不需要写到里面影响性能
//    protected void startActivityForResult(Class<?> clazz, int requestCode){
//        Intent intent = new Intent(this, clazz);
//        startActivityForResult(intent, requestCode);
//    }
}
```

# Activity管理类

```
/**
 * 类描述：Activity统一管理类<br/>
 * 创建人：吴冬冬<br/>
 * 创建时间：2018/4/12 18:39 <br/>
 */
public class ActivityUtils {
    private static volatile ActivityUtils mInstance;

    /**
     * 所有打开的Activity
     */
    private final List<Activity> mActivities;

    private ActivityUtils() {
        mActivities = new ArrayList<>();
    }

    public static ActivityUtils getInstance() {
        if (mInstance == null) {
            synchronized (ActivityUtils.class) {
                if (mInstance == null) {
                    mInstance = new ActivityUtils();
                }
            }
        }
        return mInstance;
    }

    /**
     * 添加统一管理
     */
    public void attach(Activity activity) {
        mActivities.add(activity);
    }

    /**
     * 移除解绑 - 防止内存泄漏
     *
     * @param detachActivity
     */
    public synchronized void detach(Activity detachActivity) {
        int size = mActivities.size();
        for (int i = 0; i < size; i++) {
            if (mActivities.get(i) == detachActivity) {
                mActivities.remove(i);
                size--;
                i--;
            }
        }
    }

    /**
     * 根据Activity的类名关闭 Activity
     */
    public void finish(Class<? extends Activity> activityClass) {
        for (int i = 0; i < mActivities.size(); i++) {
            Activity activity = mActivities.get(i);
            //getCanonicalName 可以获取对应包名下的activity 这样可以防止关闭不同包下相同名称的activity
            if (activity.getClass().getCanonicalName().equals(activityClass.getCanonicalName())) {
                activity.finish();
                break;
            }
        }
    }

    /**
     * 退出整个应用
     */
    public void exit() {
        int size = mActivities.size();
        for (int i = 0; i < size; i++) {
            Activity activity = mActivities.get(i);
            activity.finish();
        }
        // 杀死该应用进程
        //android.os.Process.killProcess(android.os.Process.myPid());
        //System.exit(0);
    }

    /**
     * 获取当前的Activity（最前面）
     */
    public Activity getCurrentActivity() {
        if (mActivities == null||mActivities.isEmpty()) {
            return null;
        }
        return mActivities.get(mActivities.size() - 1);
    }
}
```

