package com.dongdongwu.baseclass;

import android.app.Activity;

import java.util.ArrayList;
import java.util.List;

/**
 * 类描述：Activity统一管理类<br/>
 * 创建人：吴冬冬<br/>
 * 创建时间：2018/4/12 18:39 <br/>
 */
public class ActivityUtils {
    private static volatile ActivityUtils mInstance;

    /**
     * 所有打开的Activity
     */
    private final List<Activity> mActivities;

    private ActivityUtils() {
        mActivities = new ArrayList<>();
    }

    public static ActivityUtils getInstance() {
        if (mInstance == null) {
            synchronized (ActivityUtils.class) {
                if (mInstance == null) {
                    mInstance = new ActivityUtils();
                }
            }
        }
        return mInstance;
    }

    /**
     * 添加统一管理
     */
    public void attach(Activity activity) {
        mActivities.add(activity);
    }

    /**
     * 移除解绑 - 防止内存泄漏
     *
     * @param detachActivity
     */
    public synchronized void detach(Activity detachActivity) {
        int size = mActivities.size();
        for (int i = 0; i < size; i++) {
            if (mActivities.get(i) == detachActivity) {
                mActivities.remove(i);
                size--;
                i--;
            }
        }
    }

    /**
     * 根据Activity的类名关闭 Activity
     */
    public void finish(Class<? extends Activity> activityClass) {
        for (int i = 0; i < mActivities.size(); i++) {
            Activity activity = mActivities.get(i);
            //getCanonicalName 可以获取对应包名下的activity 这样可以防止关闭不同包下相同名称的activity
            if (activity.getClass().getCanonicalName().equals(activityClass.getCanonicalName())) {
                activity.finish();
                break;
            }
        }
    }

    /**
     * 退出整个应用
     */
    public void exit() {
        int size = mActivities.size();
        for (int i = 0; i < size; i++) {
            Activity activity = mActivities.get(i);
            activity.finish();
        }
        // 杀死该应用进程
        //android.os.Process.killProcess(android.os.Process.myPid());
        //System.exit(0);
    }

    /**
     * 获取当前的Activity（最前面）
     */
    public Activity getCurrentActivity() {
        if (mActivities == null||mActivities.isEmpty()) {
            return null;
        }
        return mActivities.get(mActivities.size() - 1);
    }
}