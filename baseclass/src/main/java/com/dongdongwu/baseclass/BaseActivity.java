package com.dongdongwu.baseclass;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

/**
 * 类描述：基础Activity<br/>
 * 创建人：吴冬冬<br/>
 * 创建时间：2018/4/12 17:44 <br/>
 */
public abstract class BaseActivity extends AppCompatActivity {

    private static final String TAG = "123===";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "--------------------------------------------------------------------");
            Log.d(TAG, "当前Activity：" + this.getClass().getSimpleName() + "---->>>onCreate()");
            Log.d(TAG, "--------------------------------------------------------------------");
        }

        //设置布局
        setContentView(getContentView());

        //初始化p层，只有mvp模式才需要
        //initPresenter();

        //初始化界面
        initView();

        //初始化头部
        initTitle();

        //初始化点击事件，如果用注解注入方式的就不需要该方法
        initEvent();

        //初始化数据
        initData();
    }

    /**
     * 初始化数据
     */
    protected abstract void initData();

    /**
     * 初始化点击事件，如果用注解注入方式的就不需要改方法
     */
    protected abstract void initEvent();

    /**
     * 初始化头部
     */
    protected abstract void initTitle();

    /**
     * 初始化界面
     */
    protected abstract void initView();

    /**
     * 设置布局
     * @return R.layout.xxx
     */
    protected abstract @LayoutRes
    int getContentView();

    /**
     * 启动activity，如果需要传餐就需要在要跳转的类中自己写一个静态方法跳转到该类
     * @param clazz
     */
    protected void startActivity(Class<?> clazz){
        Intent intent = new Intent(this, clazz);
        startActivity(intent);
    }

//    /**
//     * 启动activity，如果需要传餐就需要在要跳转的类中自己写一个静态方法跳转到该类
//     * @param clazz
//     */
//    不经常用的不需要写到里面影响性能
//    protected void startActivityForResult(Class<?> clazz, int requestCode){
//        Intent intent = new Intent(this, clazz);
//        startActivityForResult(intent, requestCode);
//    }
}